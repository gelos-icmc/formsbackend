#!/usr/bin/env python

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='gelos-forms',
    version='1.0.1',
    description='Backend do formulário de inscrição do GELOS',
    author='Guilherme Paixão',
    author_email='gui@gelos.club',
    url='https://gitlab.com/gelos-icmc/formsbackend',
    install_requires=requirements,
    packages=find_packages(include=['gelos_forms', 'gelos_forms.*']),
    entry_points={'console_scripts': ['gelos_forms=gelos_forms.__main__:main']}
)
