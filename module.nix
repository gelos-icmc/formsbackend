{ config, lib, pkgs, ... }:

with lib;
let
  inherit (pkgs.python3Packages) gunicorn;
  cfg = config.services.gelos-forms;

  inherit (config.users.users.${cfg.user}) home;

  localPostgres = cfg.database.type == "postgres" && cfg.database.createLocally;

  dbPassword =
    # Get password from file
    if cfg.database.passwordFile != null then "$(cat ${cfg.database.passwordFile})"
    # Get hardcoded password
    else cfg.database.password;
  connectionString =
    if cfg.database.type == "postgres" then
      if cfg.database.socket != null then
        # Socket connection
        "postgresql://${cfg.database.user}/${cfg.database.name}?host=${cfg.database.socket}"
      else
        # TCP Connection
        "postgresql://${cfg.database.user}:${dbPassword}@${cfg.database.host}:${toString cfg.database.port}"
    else
      "sqlite:///${cfg.database.path}";
in
{
  options.services.gelos-forms = {
    enable = mkEnableOption "gelos-forms";

    package = mkOption {
      type = types.package;
      default = pkgs.gelos-forms;
      defaultText = "pkgs.gelos-forms";
      description = "The package implementing gelos-forms";
    };

    database = {
      type = mkOption {
        type = types.enum [ "sqlite3" "postgres" ];
        example = "postgres";
        default = "sqlite3";
        description = "Database engine to use.";
      };

      host = mkOption {
        type = types.str;
        default = "localhost";
        description = "Database host address.";
      };

      port = mkOption {
        type = types.port;
        default = config.services.postgresql.port;
        defaultText = literalExpression ''
          config.${options.services.postgresql.port}
        '';
        description = "Database host port.";
      };

      name = mkOption {
        type = types.str;
        default = "gelos_forms";
        description = "Database name.";
      };

      user = mkOption {
        type = types.str;
        default = "gelos_forms";
        description = "Database user.";
      };

      password = mkOption {
        type = types.str;
        default = "";
        description = ''
          The password corresponding to <option>database.user</option>.
          Warning: this is stored in cleartext in the Nix store!
          Use <option>database.passwordFile</option> instead.
        '';
      };

      passwordFile = mkOption {
        type = types.nullOr types.path;
        default = null;
        example = "/run/keys/gelos-form-dbpassword";
        description = ''
          A file containing the password corresponding to
          <option>database.user</option>.
        '';
      };

      socket = mkOption {
        type = types.nullOr types.path;
        default = if cfg.database.createLocally then "/run/postgresql" else null;
        defaultText = literalExpression "null";
        example = "/run/postgresql";
        description = "Path to the unix socket file to use for authentication.";
      };

      path = mkOption {
        type = types.str;
        default = "${config.users.users.${cfg.user}.home}/data.db";
        defaultText = literalExpression "$HOME/data.db";
        description = "Path to the sqlite3 database file.";
      };

      createLocally = mkOption {
        type = types.bool;
        default = true;
        description = "Whether to create a local database automatically.";
      };
    };

    address = mkOption {
      type = types.str;
      default = "0.0.0.0";
      description = "Address to bind to.";
    };
    port = mkOption {
      type = types.int;
      default = 5000;
      description = "Port number to bind to.";
    };

    openFirewall = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to open port in the firewall for the server.";
    };

    user = mkOption {
      type = types.str;
      default = "gelos_forms";
      description = "Service user that will run the daemon.";
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = cfg.database.createLocally -> cfg.database.user == cfg.user;
        message = "services.gelos-forms.database.user must match services.gelos-forms.user if service.gelos-forms.database.createLocally is true";
      }
      {
        assertion = cfg.database.createLocally -> cfg.database.socket != null;
        message = "services.gelos-forms.database.socket must be set if service.gelos-forms.database.createLocally is true";
      }
    ];

    warnings = optional (cfg.database.password != "") ''
      config.services.gelos-forms.password will be stored as plaintext in the Nix store. Use database.passwordFile instead.
    '';

    # If we've been asked to create a postgres database
    services.postgresql = optionalAttrs localPostgres {
      enable = mkDefault true;
      ensureDatabases = [ cfg.database.name ];
      ensureUsers = [{
        name = cfg.database.user;
        ensurePermissions = {
          "DATABASE ${cfg.database.name}" = "ALL PRIVILEGES";
        };
      }];
    };

    systemd.services.gelos-forms =
      {
        description = "gelos-forms";
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ] ++ lib.optional localPostgres "postgresql.service";
        preStart = ''
          cat > "${home}/.env" <<EOF
          GELOS_FORMSBACKEND_DB="${connectionString}"
          EOF
          chmod 700 ${home}/.env
        '';
        environment.PYTHONPATH = "${pkgs.python3Packages.makePythonPath cfg.package.propagatedBuildInputs}/${pkgs.python3.sitePackages}:${cfg.package}/${pkgs.python3.sitePackages}";
        serviceConfig = {
          ExecStart = ''
            ${gunicorn}/bin/gunicorn \
              -b ${cfg.address}:${builtins.toString cfg.port} \
              gelos_forms:app
          '';
          Restart = "on-failure";
          User = cfg.user;
          # https://stackoverflow.com/questions/42835750
          EnvironmentFile = "-${home}/.env";
        };
      };

    users = {
      users."${cfg.user}" = {
        description = "GELOS Forms service user";
        isSystemUser = true;
        group = cfg.user;
        home = "/var/lib/${cfg.user}";
        createHome = true;
      };
      groups."${cfg.user}" = { };
    };

    networking.firewall =
      mkIf cfg.openFirewall { allowedTCPPorts = [ cfg.port ]; };
  };
}
