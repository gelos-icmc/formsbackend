{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    utils.url = "github:misterio77/flake-utils/each-default-system-map";
  };

  outputs = { self, nixpkgs, utils }: rec {
    overlays = rec {
      gelos-forms = final: prev: {
        gelos-forms = final.callPackage ./default.nix { };
      };
      default = gelos-forms;
    };

    nixosModules = rec {
      gelos-forms = import ./module.nix;
      default = gelos-forms;
    };

    packages = utils.lib.eachDefaultSystemMap (system:
      let
        pkgs = import nixpkgs { inherit system; overlays = [ overlays.default ]; };
      in
      rec {
        inherit (pkgs) gelos-forms;
        default = gelos-forms;
      });

    apps = utils.lib.eachDefaultSystemMap (system: rec {
      gelos-forms = utils.lib.mkApp { drv = packages.${system}.gelos-forms; exePath = "/bin/gelos_forms"; };
      default = gelos-forms;
    });
  };
}

