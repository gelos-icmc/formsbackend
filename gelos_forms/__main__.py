from gelos_forms import app

def main():
    app.run(port=8000)

if __name__ == "__main__":
    main()
