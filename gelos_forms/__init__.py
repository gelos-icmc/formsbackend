from flask import Flask, request, redirect
from . import model
from datetime import datetime
import random

app = Flask(__name__)

# colocar dps num arquivo
memes_list = (
    "https://youtu.be/9sJUDx7iEJw?t=18",
    "https://youtu.be/7t96m2ynKw0",
    "https://youtu.be/Og9fBTFRhDk",
    "https://youtu.be/3ia42UkRUHI",
    "https://youtu.be/upCemv2UaLc",
    "https://youtu.be/x2_OfLP8vVg",
    "https://youtu.be/JTjtsierOec",
    "https://youtu.be/nYMDFv5cKaA",
    "https://youtu.be/Fjp8nNKRiFs",
    "https://youtu.be/dQw4w9WgXcQ"
)


@app.route('/', methods=['POST'])
def receive_person():
    form = request.form
    model.save_form(form)

    return redirect('https://gelos.club/join/success', 303)

@app.route('/meme')
def getMeme():
    random.seed(datetime.now())
    return redirect(random.choice(memes_list), 303)
