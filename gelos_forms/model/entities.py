from sqlalchemy import Column
from sqlalchemy import ForeignKey, Integer, String, Text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, relationship
from .database import engine

Base = declarative_base()

class Person(Base):

    __tablename__ = 'person'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    nusp = Column(String(8), nullable=True)
    email = Column(String(50), nullable=False)
    course = Column(String(255), nullable=True)
    univ = Column(String(255), nullable=True)
    telegram = Column(String(70), nullable=False)
    
    def __init__(self, name: str, email: str, n_usp: str,
                 course: str, univ: str, telegram: str):

        self.name = name
        self.email = email
        self.nusp = n_usp
        self.course = course
        self.univ = univ
        self.telegram = telegram


class Form(Base):

    __tablename__ = 'form'

    person_id = Column(Integer, ForeignKey('person.id'), primary_key=True)
    tools = Column(Text())
    areas_interesse = Column(Text())
    distro = Column(String(30))
    fav_free_soft = Column(Text())
    fav_free_soft_desc = Column(Text())
    floss_intrst = Column(Text())
    open_source_contrib = Column(Boolean)
    project_desc = Column(Text())
    your_contrib = Column(Text())
    our_contrib = Column(Text())

    person = relationship('Person')

    def __init__(self, person: Person,
                tools: list,
                areas_interesse: list,
                distro: str,
                fav_free_soft: str,
                fav_free_soft_desc: str,
                floss_intrst: str,
                open_source_contrib: bool,
                project_desc: str,
                your_contrib: str,
                our_contrib: str):

        self.person = person
        self.tools = ';'.join(tools)
        self.areas_interesse = ';'.join(areas_interesse)
        self.distro = distro
        self.fav_free_soft = fav_free_soft
        self.fav_free_soft_desc = fav_free_soft_desc
        self.floss_intrst = floss_intrst
        self.open_source_contrib = open_source_contrib
        self.project_desc = project_desc
        self.your_contrib = your_contrib
        self.our_contrib = our_contrib

Base.metadata.create_all(engine)
