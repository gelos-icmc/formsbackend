from .entities import *
from .database import *

def save_form(form: dict):
    session = Session(engine)

    outra_area = form.get('area-outro')
    areas = form.getlist('areas')

    if outra_area is not None:
        areas.append(outra_area)

    person = Person(
                form.get('nome'),
                form.get('email'),
                form.get('nusp'),
                form.get('curso'),
                form.get('universidade'),
                form.get('telegram')
            )

    form2 = Form(
                person,
                form.getlist('knowledges'),
                areas,
                form.get('distro'),
                form.get('fav-soft'),
                form.get('fav-soft-just'),
                form.get('interessa-floss'),
                form.get('projeto')=='sim',
                form.get('sobre-projetos'),
                form.get('you-contribute'),
                form.get('we-contribute')
            )
    
    session.add(person)
    session.commit()
    session.add(form2)
    session.commit()
