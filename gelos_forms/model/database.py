from sqlalchemy import create_engine
from os import environ

DEFAULT_DB = 'sqlite:///gelos.db'


if 'GELOS_FORMSBACKEND_DB' in environ:
    engine = create_engine(environ['GELOS_FORMSBACKEND_DB'])

else:
    engine = create_engine(DEFAULT_DB)
