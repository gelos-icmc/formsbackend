# Backend Formulário GELOS

Este é um simples backend para o formulário de inscrição do GELOS.

Feito com Python, Flask e SQLAlchemy.

## Como usar

### Com pip
Requer:
- Python 3.8+
- PyPi

Caso queira usar venv (recomendado):
```bash
pip install virtualenv
python -m venv venv
source venv/bin/activate      # Bash
# Ou
source venv/bin/activate.fish # Fish
# Devem ter outras pra outras shells
```

Instale o pacote com o pip:
```bash
pip install .
```

Execute o programa:
```bash
gelos_forms
```

### Com nix
Basta usar:
```bash
nix run
```

Também temos um módulo do NixOS, que usa boas prática de deployment (com wsgi).

Basta incluir o flake input. Exemplo de uso:
```nix
{ gelos-forms, pkgs, ... }: {
  imports = [ gelos-forms.nixosModules.default ];

  services.gelos-forms.backend = {
    enable = true;
    # Parâmetros opcionais e seus valores padrão
    # packages = pkgs;gelos-forms; # Permite overridar o pacote
    # database = "sqlite:////var/lib/gelos-forms/data.db";
    # port = 5000;
    # host = "0.0.0.0";
    # Recomendo usar proxy reverso ao invés de expor diretamente, mas se quiser pode
    # openFirewall = false;
  };
}
```


## Como desenvolver

### Com pip

(Crie e ative sua virtualenv igual acima, caso queira usar)

Instale as dependências:
```bash
pip install -r .
```

Execute com:
```bash
python -m gelos_forms
```

### Com nix
Entre no ambiente dev:
```bash
nix develop
```

E execute com:
```bash
python -m gelos_forms
```
