{ python3Packages, ... }: python3Packages.buildPythonPackage {
  name = "gelos-forms";
  version = "1.0.1";
  src = ./.;
  propagatedBuildInputs = with python3Packages; [
    click
    flask
    greenlet
    itsdangerous
    jinja2
    markupsafe
    psycopg2
    setuptools
    sqlalchemy
    werkzeug
  ];
}
